import {Category} from './category';
import { Categories } from "./categories";

// Prepare the test dataset


describe('Categories', () => {
  const root = new Category({ id: -1 });
  
  beforeEach(() => {
    root.children = [];
    
    const cat101 = new Category({
      id: 101,
      name: "Accounting",
      keywords: "Taxes"
    });

    const cat100 = new Category({
      id: 100,
      name: "Business",
      keywords: "Money"
    });
    cat100.add(cat101);
    root.add(cat100);
    
    // const cat200 = new Category({
    //   id: 200,
    //   name: "Tutoring",
    //   keywords: "Teaching"
    // });
    // root.add(cat200);

    
    // const cat103 = new Category({ id: 103, name: "Corporate Tax" });
    // const cat109 = new Category({ id: 109, name: "Small Business Tax" });
    // cat101.add(cat103);
    // cat101.add(cat109);

    // const cat102 = new Category({
    //   id: 102,
    //   name: "Taxation"
    // });
    // cat100.add(cat101);
    // cat100.add(cat102);
    

    // const cat201 = new Category({ id: 201, name: "Computer" });
    // const cat202 = new Category({ id: 202, name: "Operating System" });
    // cat201.add(cat202);
    // cat200.add(cat201);
    
    
    // root.add(cat100);
    // root.add(cat200);
  });
  
  test("can add root category", () => {
    const root = new Category({ id: -1 });
    const categories = new Categories(root);
    expect(categories.root).toEqual(root);
  });
  
  test("can add second level categories", () => {
    const categories = new Categories(root);
    // expect(categories.root.children.length).toEqual(2);
    console.log(categories.root.children[0]);
    // expect(categories.root.children[0].children.length).toEqual(2);
    
  });
});
