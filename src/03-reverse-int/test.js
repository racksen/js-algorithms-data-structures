import ReverseInt from './index';

it('should reverse the integer value correctly', () => {
  expect(ReverseInt.reverse_1(15)).toEqual(51)
});

it('should reverse the integer value correctly', () => {
  expect(ReverseInt.reverse_1(-50)).toEqual(-5)
});