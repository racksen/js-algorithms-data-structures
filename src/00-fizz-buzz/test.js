import fizzBuzz from './index'

it('should fizzbizz correctly', () => {
  let output = fizzBuzz(20) 
  let expectArray = [output[1], output[3], output[5], output[15]] 
  let toEqualArray = [1, 'Fizz', 'Buzz', 'FizzBuzz']
  expect(expectArray).toEqual(toEqualArray)
});