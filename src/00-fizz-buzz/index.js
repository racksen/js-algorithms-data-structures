const temp = (num) => {
  return (
  (num % 3 === 0 && num % 5 === 0) ? 'FizzBuzz' :
    (num !== 0 && num % 3 === 0) ? 'Fizz' :
      (num !== 0 && num % 5 === 0) ? 'Buzz' : 
        (num)
  )
}
// const fizz_buzz = (num) => {
//   return num % 3 === 0 && num % 5 === 0 ? 'FizzBuzz' : num
// }

// const fizz = (num) => {
//   return (typeof num !== "number") ? num : (num % 3 === 0 ? 'Fizz' : num)
// }

// const buzz = (num) => {
//   return (typeof num !== "number") ? num : ( num % 5 === 0 ? 'Buzz' : num)
// }

const fizzBuzz = (num) => {
  return Array(num).fill().map((_, n) => temp(n))
  // Array(num).fill().forEach((_, n) => console.log(temp(n)))

}

export default fizzBuzz