import {Node, Tree} from './index';

describe('Node', () => {
  test('Node has a constructor', () => {
    expect(typeof Node.prototype.constructor).toEqual('function');
  });

  test('Node has data and children properties', () => {
    const n= new Node(-1);
    expect(n.data).toEqual(-1);
    expect(n.children.length).toEqual(0);
  });
  test("Node can add children", () => {
    const n = new Node(-1);
    n.add(100);
    n.add(200);
    expect(n.data).toEqual(-1);
    expect(n.children.length).toEqual(2);
  });
  test("Node can remove children", () => {
    const n = new Node(-1);
    n.add(100);
    n.add(200);
    expect(n.children.length).toEqual(2);
    n.remove(200);
    expect(n.children.length).toEqual(1);

  });
  
});


describe('Tree', () => {
  test('starts empty', () => {
    const t = new Tree(null);
    expect(t.root).toEqual(null);
  });
  test("Can traverse bf", () => {
     const letters = [];
     const t = new Tree();
     t.root = new Node("a");
     t.root.add("b");
     t.root.add("c");
     t.root.children[0].add("d");

     t.traverseBF(node => {
       letters.push(node.data);
     });

     expect(letters).toEqual(["a", "b", "c", "d"]);
   });
  test("Can traverse df", () => {
    const letters = [];
    const t = new Tree();
    t.root = new Node("a");
    t.root.add("b");
    t.root.add("d");
    t.root.children[0].add("c");

    t.traverseDF(node => {
      letters.push(node.data);
    });

    expect(letters).toEqual(["a", "b", "c", "d"]);
  });
});