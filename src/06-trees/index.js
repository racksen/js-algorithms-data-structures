class Node {
  constructor(data) {
    this.data = data;
    this.children = [];
  }

  add(data) {
    this.children.push(new Node(data));
  }

  remove(data) {
    this.children = this.children.filter(n => n.data === data);
  }
  
}

class Tree {
  constructor(node) {
    this.root = node;
  }

  traverseBF(fn) {
    const container = [this.root];
    while(container.length) {
      const node = container.shift();
      container.push(...node.children);
      fn(node);
    };

  }
  traverseDF(fn) {
    const container = [this.root];
    while(container.length) {
      const node = container.shift();
      container.unshift(...node.children);
      fn(node);
    };
    
  }
}

export {Node, Tree};