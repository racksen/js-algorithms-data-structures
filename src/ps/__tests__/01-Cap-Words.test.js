import {toWords, toTitleCase} from "../01-Cap-Words";
describe('capitalize the words in a sentence', () => {
    it('should make array of words', () => {
        const sentence = 'capitalize the words in a sentence';
        expect(toWords(sentence)).toEqual(sentence.split(' '));
    });
    it('should title case the sentence', () => {
        const sentence = 'i love soLVinG prOblems and it is fun';
        const expectToEqual = 'I Love Solving Problems and It Is Fun';
        expect(toTitleCase(sentence)).toBe(expectToEqual);
    });
});