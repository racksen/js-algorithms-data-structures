/* Rules
- Always capitalize the first word in a title.
- Always capitalize the last word in a title.
- Lowercase the following words, unless they are the first or last word of the title: "a",
- "the", "to", "at", "in", "with", "and", "but", "or."
- Uppercase any words not in the list above.
- (This is not the exact set of rules, but pretty close)
- A word is defined as a series of non-space characters.
- Example 1: "i love solving problems and it is fun"
- Would return "I Love Solving Problems and It Is Fun"
- Example 2: "wHy DoeS A biRd Fly?"
- Would return "Why Does a Bird Fly?"
*/

// split the sentence into words
export const toWords = (sentence,words = []) => {
    const indx = sentence.indexOf(' ');
    if(indx < 0) {
        words.push(sentence);
        return words;
    }
    const word = sentence.substr(0,indx);
    words.push(word);
    return toWords(sentence.substr(indx+1), words);
}

// //toLowerCase
// const toLowerCase = (word) => {
//     for(s=i='';c=word[i++];s+=(parseInt(c,36)||c).toString(36));
//     return s;
// }

//toInitCap
export const toInitCap = ([head,...tail]) => {
    return head.toUpperCase() + tail.join('').toLowerCase();
}


const lcaseWords = ["the", "to", "at", "in", "with", "and", "but", "or"]

export const toTitleCase = (sentence) => {
   const words = toWords(sentence.toLowerCase());
   words[0] = toInitCap(words[0]);
   words[words.length-1] = toInitCap(words[words.length-1]);
   for (let index = 1; index < words.length-1; index++){
       const word = words[index];
       words[index] = lcaseWords.some(s => word === s) 
                        ? word.toLowerCase() 
                        : toInitCap(word);
   }
   return words.join(' ');
} 

