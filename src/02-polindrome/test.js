import polindrome from './index'


it('should fail when the given string is not polindrome', () => {
  expect(polindrome.polindrome_1('hello')).toEqual(false)
});

it('should pass when the given string is polindrome', () => {
  expect(polindrome.polindrome_2('abba')).toEqual(true)
});