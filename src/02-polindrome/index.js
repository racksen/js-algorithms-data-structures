const polindromeObj = {
  polindrome_1 : (str) => {
    return str.split('').reverse().join('') == str
  },
  polindrome_2: (str) => {
    return str.split('').every((char,i) => {
      return char == str[str.length - i - 1]
    })
  }

}

export default polindromeObj