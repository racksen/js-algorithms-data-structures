const anagram = {
  
  char_map: (str) => {
    const obj = {}
    str.split('').forEach((e) => {
      obj[e] = obj[e] + 1 || 1
    })
    return obj;
  },
  
  max_char_1 : (str) => {

    let max = 0
    let max_char = ''
    let charObj = anagram.char_map(str)
    for (let key in charObj) {
      if (charObj[key] > max) {
        max = charObj[key]
        max_char = key 
      }
    }
    return max_char
  }
}

export default anagram;