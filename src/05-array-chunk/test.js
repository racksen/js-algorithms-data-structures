import ArrayChunk from './index';

it('should chunk the array', () => {
  expect(ArrayChunk.chunk_1([1,2,3,4], 3)).toEqual([[1,2,3],[4]])
});