const arrayChunk = {
  chunk_1: (arr, chnkSize) => {
    let chunked = []
    let index = 0
    while (index < arr.length) {
      chunked.push(arr.slice(index, index + chnkSize))
      index += chnkSize      
    }
    return chunked
  }
}

export default arrayChunk