const levelWidth = (root) => {
  let container = [root, 'STOPPER'];
  const counter= [0];

  while (container.length > 1) {
    const node = container.shift();
    if (node === 'STOPPER') {
      counter.push(0);
      container.push(node);
    } else {
      container.push(...node.children);
      counter[counter.length - 1]++;
    }
  }
  return counter;
}

module.exports = levelWidth;
