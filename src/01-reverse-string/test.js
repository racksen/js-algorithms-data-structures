import ReverseString from './index';

it('should reverse with the function reverse_1 logic', () => {
  expect(ReverseString.reverse_1("hello")).toEqual('olleh')
});

it('should reverse with the function reverse_2 logic', () => {
  expect(ReverseString.reverse_2("hello")).toEqual('olleh')
});

it('should reverse with the function reverse_3 logic', () => {
  expect(ReverseString.reverse_3("senthil")).toEqual('lihtnes')
});
