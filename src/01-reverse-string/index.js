const ReverseString = {
  
  reverse_1: (str) => {
    return str.split('').reverse().join('')
  },

  reverse_2: (str) => {
    let retStr = ''
    str.split('').forEach(char => retStr = char + retStr );
    return retStr
  },

  reverse_3: (str) => {
    return str.split('').reduce((acc, char) => char + acc)
  }

}
export default ReverseString
